package training.web;

public class Person {
	
	private final int iD;
	private final String name;
	
	public Person(int iD, String name) {
		this.iD=iD;
		this.name=name;
	}
	
	public int getiD() {
		return iD;
	}
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return String.format("Id : %d, Name : %s", this.getiD(), this.getName());
	}

}