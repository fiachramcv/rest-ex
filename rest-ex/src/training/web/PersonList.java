package training.web;

// Indicator Interface
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.SessionScoped;

@SessionScoped
public class PersonList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Person> people = new ArrayList<>();

	public PersonList() {
	}

	public void addPerson(Person person) {
		for (Person p : people) {
			if (p.getiD() == person.getiD()) {
				throw new IllegalArgumentException("Cannot add dupicate iD's");
			}
		}
		people.add(person);
	}

	public Person removePerson(Person person) {
		Person found = null;
		for (Person p : people) {
			if ((p.getiD() == person.getiD()) && (p.getName().equals(person.getName()))) {
				people.remove(p);
				found = p;
				break;
			}
		}
		if (found == null) {
			throw new IllegalArgumentException("Person does not exist");
		}
		return found;
	}

	public int getSize() {
		return people.size();
	}

	public Person[] getPersons() {
		return people.toArray(new Person[people.size() - 1]);
	}

	public void sortListByName() {
		people.sort(new Comparator<Person>() {
			@Override
			public int compare(Person lhs, Person rhs) {
				return lhs.getName().compareTo(rhs.getName());
			}
		});
	}

	public void sortListByID() {
		people.sort(new Comparator<Person>() {
			@Override
			public int compare(Person lhs, Person rhs) {
				return lhs.getiD() - rhs.getiD();
			}
		});
	}

	public String longestName() {
		List<Person> copy = new ArrayList<>(people);
		copy.sort(new Comparator<Person>() {
			@Override
			public int compare(Person lhs, Person rhs) {
				return lhs.getName().length() - rhs.getName().length();
			}
		});
		return copy.get(copy.size() - 1).getName();
	}
	
	@Override
	public String toString() {
		return people.toString();
	}
}
