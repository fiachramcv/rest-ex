package training.web;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/")
public class SimpleController {
	
	@Inject
	private PersonList personlist;
	
	@POST @Path("/addPerson")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String addPerson(@FormParam("id") int id, @FormParam("name") String name) {
		personlist.addPerson(new Person(id, name));
		return String.format("Id : %d Name : %s added to list", id, name);
	}
	
	@POST @Path("/removePerson")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String removePerson(@FormParam("id") int id, @FormParam("name") String name) {
		personlist.removePerson(new Person(id, name));
		return String.format("Id : %d Name : %s removed from list", id, name);
	}
	
	@GET
	@Path("/getList")
	public String getList() {
		return personlist.toString();
	}
	
	@GET @Path("/addPerson/{id}/{name}")
	public String addPersonPath(@PathParam("id") int id, @PathParam("name") String name) {
		personlist.addPerson(new Person(id, name));
		return String.format("Id : %d Name : %s added to list", id, name);
	}
	
	@GET
	@Path("/sortListByName")
	public String sortListByName() {
		personlist.sortListByName();
		return personlist.toString();
	}
	
	@GET
	@Path("/sortListById")
	public String sortListById() {
		personlist.sortListByID();
		return personlist.toString();
	}
	
	@GET @Path("/addPersonQuery")
	public String speak(@QueryParam("id") int id, @QueryParam("name") String name) {
		personlist.addPerson(new Person(id, name));
		return String.format("Id : %d Name : %s added to list", id, name);
	}
	
}
